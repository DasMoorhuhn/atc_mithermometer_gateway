# This is the build stage for getting all python libs. Mainly it's for reducing the final image size because for building and installing the pips, many tools are needed which are just bloat to the final image.
FROM python:3.12-alpine3.20 AS pip_build_stage

COPY ./python/requierements.txt /

RUN apk add \
    make \
    git \
    glib-dev \
    gcc \
    build-base \
    freetype-dev \
    libpng-dev \
    openblas-dev \
    libxml2-dev \
    libxslt-dev

# BluePy needs to be build here... idk why but pip install fails on alpine somehow
RUN git clone https://github.com/IanHarvey/bluepy.git && \
    cd bluepy && \
    python3.12 setup.py build && \
    python3.12 setup.py install && \
    cd /

# Normal pip install for pyyaml failed on RPI4, so I build it here
RUN git clone https://github.com/yaml/pyyaml.git && \
    cd pyyaml && \
    python3.12 setup.py build && \
    python3.12 setup.py install && \
    cd /

RUN pip3.12 install -r requierements.txt


# This is the stage for the final image
FROM python:3.12-alpine3.20

WORKDIR /src
COPY ./python/src/ .
COPY ./python/docker_entrypoint.sh /
RUN mkdir -p data/log
VOLUME /src/data

RUN apk add --no-cache sudo bluez tzdata
ENV TZ=Europe/Berlin
ENV DOCKER=true
ENV API=false
ENV NAME=ATC_MiThermometer_Gateway
ENV VERSION=24-08-29
ENV MODE=1

# Copy pips from the pip build stage
COPY --from=pip_build_stage /usr/local/lib/python3.12/site-packages /usr/local/lib/python3.12/site-packages
COPY --from=pip_build_stage /usr/local/bin /usr/local/bin

ENTRYPOINT sh /docker_entrypoint.sh
