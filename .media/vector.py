import matplotlib.pyplot as plt

# Create a new figure
fig, ax = plt.subplots(figsize=(6, 6))

# Hide the axes
ax.axis('off')

# Add the text and symbols from the display
# Temperature
ax.text(0.5, 0.75, '25.8', fontsize=120, ha='center', va='center', fontweight='bold')
# Degree Celsius symbol
ax.text(0.85, 0.75, '°C', fontsize=60, ha='center', va='center', fontweight='bold')
# Humidity
ax.text(0.5, 0.5, '36%', fontsize=120, ha='center', va='center', fontweight='bold')
# Face symbol
ax.text(0.5, 0.25, '( ^_^ )', fontsize=80, ha='center', va='center', fontweight='bold')
# Save as a vector graphic
plt.savefig('display_vector_graphic_with_space.png', format='png')

# Show the plot
plt.show()

