#!/bin/sh

env > .env

if [ "$API" = true ]; then
  python3.12 api_endpoints.py &
  sleep 1
fi

python3.12 start_discovery_server.py &
python3.12 main.py
