import os
from flask import Flask
from flask import jsonify
from flask import send_from_directory
from flask_cors import CORS
from flask_cors import cross_origin
from check_update import check_for_update


class API:
  """
  API endpoints
  """

  def __init__(self):
    self.app = Flask(import_name='backend', static_folder='/data', static_url_path='')
    CORS(self.app)
    self.app.config['CORS_HEADERS'] = 'Content-Type'

    # --------Static Routes-------
    @self.app.route('/api')
    @cross_origin()
    def serve_root():
      workdir, filename = os.path.split(os.path.abspath(__file__))
      update = check_for_update()
      root_dict = {
        "version": {
          "version": os.getenv('VERSION'),
          "update_available": update.update_available if update is not None else None,
          "up_to_date": update.up_to_date if update is not None else None,
          "develop_version": update.development if update is not None else None,
        },
        "mode": os.getenv('MODE'),
        "name": os.getenv('NAME'),
        "info": {
          "files_size_sum": self.get_file_size(),
          "files": os.listdir(f'{workdir}/data')
        }
      }
      return jsonify(root_dict)

    @self.app.route('/api/json/<path:path>')
    @cross_origin()
    def serve_json(path):
      return send_from_directory(f'{workdir}/data', path)

    @self.app.route('/charts')
    @cross_origin()
    def serve_index():
      return send_from_directory('/src', 'chart.html')

  # --------Helpers-------
  def get_file_size(self):
    workdir, filename = os.path.split(os.path.abspath(__file__))
    files = os.listdir(f'{workdir}/data')
    sizes = 0
    for file in files:
      if file.endswith('.json'): sizes += os.path.getsize(f'{workdir}/data/{file}')
    return sizes


api = API()
api.app.run(host='0.0.0.0', port=8000)
