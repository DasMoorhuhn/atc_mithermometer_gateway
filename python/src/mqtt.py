import paho.mqtt.client as MQTT

topic_root = "/atc_mithermometer_gateway"

mqtt = MQTT.Client(mqtt.CallbackAPIVersion.VERSION2)
mqtt.connect("192.168.178.140", 1883, 60)


def publish_measurement(mac):
  topic = f"{topic_root}/measurements/{mac}"
  topic_temp = f"{topic}/temperature"
  topic_humid = f"{topic}/humidity"
  topic_battery = f"{topic}/battery"

  mqtt.publish(topic_temp, )
