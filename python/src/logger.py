import os
import logging


def get_logger(logger_name:str, log_file='gateway.log'):
  logger_name = logger_name.replace('__', '')
  debug_level = os.getenv('DEBUG').upper()
  if debug_level not in ('CRITICAL', 'ERROR', 'WARNING', 'WARN', 'INFO', 'DEBUG', 'NOTSET', 'FATAL'):
    print(f'Loglevel "{debug_level}" is not supported.')
    exit(0)
  logger = logging.getLogger(logger_name)
  logger.setLevel(logging.getLevelName(debug_level))
  handler = logging.FileHandler(filename=f'data/{log_file}', encoding='utf-8', mode='a')
  formatter = logging.Formatter('%(asctime)s|%(levelname)s|%(name)s|:%(message)s')
  handler.setFormatter(formatter)
  stream_handler = logging.StreamHandler()
  stream_handler.setFormatter(formatter)
  logger.addHandler(stream_handler)
  logger.addHandler(handler)
  logger.info(f"Logger {logger_name} init done")
  return logger
