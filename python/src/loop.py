from time import sleep
from log_data import log_to_json
from ble_discovery import start_discovery
from logger import get_logger
logger = get_logger(__name__)


def start_loop(interval=40, timeout=20):
  logger.info(f"Starting loop with interval {interval}s")
  while True:
    devices = start_discovery(timeout=timeout)
    log_to_json(devices)
    sleep(interval)
