import yaml
import os

workdir, filename = os.path.split(os.path.abspath(__file__))
config_file = f"{workdir}{os.sep}devices.yml"


class Device:
  def __init__(self, data):
    self.mac = data['mac']
    self.name = data['name']
    self.room = data['room']

  def to_json(self):
    return {
      "mac": self.mac,
      "name": self.name,
      "room": self.room
    }


def get_devices():
  devices_list = []
  with open(file=config_file, mode='r') as file:
    devices = yaml.safe_load(file)
    for device in devices['devices']: devices_list.append(Device(data=device))
    return devices_list


def get_device(dev):
  return next((d for d in get_devices() if d.mac == dev.addr.upper()), None)
