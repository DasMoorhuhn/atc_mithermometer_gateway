import os
import json
import requests
from logger import get_logger

logger = get_logger(__name__)


class State:
  def __init__(self, version_int:int):
    self.version_int:int = version_int
    self.version_str:str = f"{str(version_int)[0]}{str(version_int)[1]}-{str(version_int)[2]}{str(version_int)[3]}-{str(version_int)[4]}{str(version_int)[5]}"
    self.update_available:bool = False
    self.up_to_date:bool = False
    self.development:bool = False


class Release:
  def __init__(self, data:dict):
    self.name = data['name']
    self.tag_name = data['tag_name']
    self.description = data['description']
    self.created_at = data['created_at']
    self.released_at = data['released_at']
    self.upcoming_release = data['upcoming_release']
    self.version_int = int(self.tag_name.replace("-", ""))


def check_for_update():
  try: version_current = int(os.getenv('VERSION').replace("-", ""))
  except:
    logger.error("Error getting current version")
    return
  project_id = 58341398
  request = f"https://gitlab.com/api/v4/projects/{project_id}/releases"
  response = requests.get(url=request, timeout=1)
  if not response.ok: return

  releases_json = json.loads(response.text)
  # Date, Object
  latest = [0, None]
  for release_json in releases_json:
    release = Release(release_json)
    if release.version_int > latest[0]:
      latest[0] = release.version_int
      latest[1] = release
    logger.debug(repr(latest))

  release = latest[1]
  if release.version_int > version_current:
    state = State(release.version_int)
    state.update_available = True
    return state

  elif release.version_int == version_current:
    state = State(release.version_int)
    state.up_to_date = True
    return state

  else:
    state = State(release.version_int)
    state.development = True
    return state


def print_state(state:State):
  if state is None: return
  logger.info(f"Current version: {os.getenv('VERSION')}")
  if state.update_available:
    logger.info(f"Update available: {state.version_str}")
  if state.up_to_date:
    logger.info(f"Up to date")
  if state.development:
    logger.info(f"Development Version")
