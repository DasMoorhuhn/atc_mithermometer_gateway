import os
from logger import get_logger
from ble_discovery import start_discovery
from log_data import log_to_json
from loop import start_loop
from check_update import check_for_update
from check_update import print_state

INTERVAL = 40
TIMEOUT = 20
DOCKER = os.getenv('DOCKER') == 'true'
DEBUG = os.getenv('DEBUG')
interval = os.getenv('LOOP')
timeout = os.getenv('TIMEOUT')

logger = get_logger(__name__)

logger.debug(f"INTERVAL: {INTERVAL}")
logger.debug(f"TIMEOUT: {TIMEOUT}")
logger.debug(f"interval: {interval}")
logger.debug(f"timeout: {timeout}")
logger.debug(f"DOCKER: {DOCKER}")
logger.debug(f"DEBUG: {DEBUG}")
logger.debug(f"VERSION: {os.getenv('VERSION')}")

update_state = check_for_update()
print_state(update_state)

try:
  if DOCKER:
    logger.info('Running in Docker')

    try:INTERVAL = int(interval)
    except:pass

    try:TIMEOUT = int(timeout)
    except:pass

    if interval is None: log_to_json(start_discovery(timeout=TIMEOUT))
    else:start_loop(INTERVAL, TIMEOUT)

  else:
    start_loop(interval=40)
except Exception as err:
  logger.error(err)
