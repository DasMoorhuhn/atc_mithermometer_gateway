import datetime
import time


def get_time_now():
  date_now = datetime.datetime.now()
  return str(date_now).split(" ")[0]


def get_unix_time():
  return time.time()


def get_date():
  date_now = datetime.datetime.now()
  return str(date_now).split(" ")
