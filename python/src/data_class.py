from datetime import datetime


class Data:
  def __init__(self, data):
    self.timestamp = None
    self.mac = None
    self.temperature = None
    self.humidity = None
    self.battery_percent = None
    self.battery_volt = None
    self.count = None
    self.parse_data(data)

  def parse_data(self, val):
    data_bytes = [int(val[i:i + 2], 16) for i in range(0, len(val), 2)]
    if data_bytes[8] > 127: data_bytes[8] -= 256

    self.timestamp = datetime.now().astimezone().replace(microsecond=0).isoformat()
    self.mac = ":".join(["{:02X}".format(data_bytes[i]) for i in range(2, 8)]).upper()
    self.temperature = (data_bytes[8] * 256 + data_bytes[9]) / 10
    self.humidity = data_bytes[10]
    self.battery_percent = data_bytes[11]
    self.battery_volt = (data_bytes[12] * 256 + data_bytes[13]) / 1000
    self.count = data_bytes[14]

  def print_data(self):
    print(self.to_json())

  def to_json(self):
    return {
      "timestamp": self.timestamp,
      "mac": self.mac,
      "temperature": self.temperature,
      "humidity": self.humidity,
      "battery_percent": self.battery_percent,
      "battery_volt": self.battery_volt,
      "count": self.count,
    }
