import os
import sys
import json
from data_class import Data
from devices import Device
from logger import get_logger
logger = get_logger(__name__)


def log_to_json(devices):
  workdir, filename = os.path.split(os.path.abspath(__file__))

  for device in devices:
    dev, data_obj, from_config = device
    data_obj: Data
    from_config: Device
    file_name = f'{workdir}/data/{str(data_obj.mac).replace(":", "-")}.json'
    logger.debug(f"Save to {file_name}")

    try:
      with open(file_name, 'r') as file: data = json.load(file)
    except:
      with open(file_name, 'w') as file: file.write("[]")
      data = []

    measurements = {
      "timestamp": data_obj.timestamp,
      "temperature": data_obj.temperature,
      "humidity": data_obj.humidity,
      "battery_percent": data_obj.battery_percent,
      "battery_volt": data_obj.battery_volt,
      "rssi": dev.rssi,
      "name": from_config.name if from_config is not None else "Unknown",
      "room": from_config.room if from_config is not None else "Unknown"
    }
    data.append(measurements)

    logger.debug(measurements)

    with open(file_name, 'w') as file: file.write(json.dumps(data, indent=2))


def log_to_mongodb(data):
  pass


def log_to_mqtt(data):
  pass

