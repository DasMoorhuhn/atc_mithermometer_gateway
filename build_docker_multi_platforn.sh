TAG=latest
IMAGE=dasmoorhuhn/atc-mithermometer-gateway
PLATFORMS=linux/amd64,linux/arm64,linux/arm
HELP="USAGE: sh build_docker.sh \n
[ -t | --tag ] Select a tag for building. Default: latest \n
[ -i | --image ] Select image tag for building. Default: dasmoorhuhn/atc-mithermometer-gateway \n
[ -p | --platforms ] Select the platforms, for which the image should build. Default: linux/amd64,linux/arm64,linux/arm \n
[ -h | --help ] Get this dialog"

docker buildx version
if [ "$?" != 0 ]; then
  echo Missing docker buildx. Please install docker buildx from https://github.com/docker/buildx and try build again.
  exit 1
fi

create_builder() {
  docker buildx create --name builder
  docker buildx use builder
}

build_docker() {
  create_builder
  docker login
  docker buildx build --tag $IMAGE:$TAG --platform=$PLATFORMS --push .
}

while [ "$1" != "" ]; do
  case $1 in
    -t | --tag )
      shift
      TAG=$1
      shift
      ;;
    -i | --image )
      shift
      IMAGE=$1
      shift
      ;;
    -p | --platforms )
      shift
      PLATFORMS=$1
      shift
      ;;
    -h | --help )
      echo $HELP
      exit
      ;;
    * )
      echo $HELP
      echo $1
      exit 1
  esac
done

build_docker