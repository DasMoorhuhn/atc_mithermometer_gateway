TAG="latest"
CONTAINER="dasmoorhuhn/atc-mithermometer-gateway"
CONTAINER_NAME="ATC_MiThermometer_Gateway"
VOLUME=$(pwd)/data

HELP="Using any command line argument except -d bypasses the .env file\n\n
USAGE: sh run_docker.sh [OPTIONS] \n
[ -d ] Run in Backgrund \n
[ -t | --tag ] Set a docker tag. Default: latest \n
[ -b | --build ] Build the image before running the container \n
[ -l | --loop ] Start the gateway in looping mode. e.g.: --loop 40 will set the interval of the loop to 40s. Default is single run mode \n
[ -i | --interactive ] Start the container in interactive mode. That means, you can read the console output in real time and abort via STRG+C \n
[ -a | --api ] Start with the API \n
[ -v | --volume ] Set the volume, where the data from the gateway will be stored. Use relative path like /home/user/gateway/data \n
[ -n | --name ] Set a custom name for this gateway \n
[ -m2 | --mesh-gateway ] Set the mode to mesh gateway. This gateway is meant to expand the bluetooth radius. Default mode is main gateway \n
[ -tz | --timezone ] Set the timezone. Default is Europe/Berlin \n
[ -to | --timeout ] Set the timeout for the bluetooth scan. default is 20s \n
[ -h | --help ] Get this dialog \n
[ --debug ] Set into debug mode"

docker version > /dev/null 2>&1
if [ "$?" != 0 ]; then
  echo Missing docker. Please install docker and try build again.
  exit 1
fi


check_for_devices_config() {
  if [ ! -f devices.yml ]; then
    touch devices.yml
    echo 'devices:
  - mac: A4:C1:38:00:00:00
    name: "my_room"
    room: "my_room"' >> devices.yml
  fi
}


docker_run() {
  sudo killall -9 bluetoothd > /dev/null 2>&1
  echo Killing old container...
  docker stop $CONTAINER_NAME > /dev/null 2>&1
  docker container rm $CONTAINER_NAME > /dev/null 2>&1

  check_for_devices_config

  if [ "$SKIP_ENV" = true ]; then
    ENV_EXISTS=false

  else
    if [ -e .env ]
    then
      echo Loading .env file
      export $(cat .env | xargs)
      ENV_EXISTS=true
    else
      echo No env file found
      ENV_EXISTS=false

      TIME_ZONE=""
      INTERACTIVE=true
      BUILD=true
      API=true
      DEBUG="INFO"
      MODE="1"
      LOOP="40"
      TIMEOUT="20"
    fi
  fi

  COMMAND="docker run $BACKGROUND"
  COMMAND="$COMMAND --cap-add=SYS_ADMIN"
  COMMAND="$COMMAND --cap-add=NET_ADMIN"
  COMMAND="$COMMAND --net=host"
  COMMAND="$COMMAND --name=$CONTAINER_NAME"
  COMMAND="$COMMAND --restart=on-failure"
  COMMAND="$COMMAND --volume=/var/run/dbus/:/var/run/dbus/"
  COMMAND="$COMMAND --volume=$VOLUME:/src/data"
  COMMAND="$COMMAND --volume=$PWD/devices.yml:/src/devices.yml"

  if [ "$ENV_EXISTS" = true ]; then
    COMMAND="$COMMAND --env-file .env"
  fi

  if [ "$BACKGROUND" = "--detach" ]; then
    INTERACTIVE=false
  fi

  if [ "$INTERACTIVE" = true ]; then
    COMMAND="$COMMAND --interactive"
    COMMAND="$COMMAND --tty"
    COMMAND="$COMMAND --attach=stdout"
    COMMAND="$COMMAND --attach=stdin"
  fi

  if [ "$LOOP" != "0" ]; then
    COMMAND="$COMMAND --env LOOP=$LOOP"
  fi

  if [ "$TIMEOUT" != "0" ]; then
    COMMAND="$COMMAND --env TIMEOUT=$TIMEOUT"
  fi

  if [ "$BUILD" = true ]; then
    sh build_docker.sh --tag $TAG
  fi

  if [ "$TIME_ZONE" != "" ]; then
    COMMAND="$COMMAND --env TZ=$TIME_ZONE"
  fi

  if [ "$API" != false ]; then
    COMMAND="$COMMAND --env API=$API"
  fi

  if [ "$MODE" != 1 ]; then
    COMMAND="$COMMAND --env MODE=$MODE"
  fi

  if [ "$NAME" != "" ]; then
    COMMAND="$COMMAND --env NAME=$NAME"
  fi

  if [ "$DEBUG" = "DEBUG" ]; then
    COMMAND="$COMMAND --env DEBUG=$DEBUG"
    COMMAND="$COMMAND $CONTAINER:$TAG"
    echo
    echo $COMMAND
    echo
    echo DEBUG MODE
  else
    COMMAND="$COMMAND --env DEBUG=$DEBUG"
    COMMAND="$COMMAND $CONTAINER:$TAG"
  fi

  echo Start container...
  echo
  $COMMAND

  docker container rm $CONTAINER_NAME > /dev/null 2>&1
}

while [ "$1" != "" ]; do
  case $1 in
    -se | --skip-env-file )
      SKIP_ENV=true
      echo "Skip env file"
      shift
      ;;
    -d )
      BACKGROUND="--detach"
      shift
      ;;
    --debug )
      shift
      DEBUG=$1
      shift
      ;;
    -a | --api)
      API=true
      shift
      ;;
    -b | --build )
      BUILD=true
      shift
      ;;
    -v | --volume )
      shift
      VOLUME=$1
      shift
      ;;
    -n | --name )
      shift
      NAME=$1
      shift
      ;;
    -m2 | --mesh-gateway)
      MODE=2
      shift
      ;;
    -tz | --timezone )
      shift
      TIME_ZONE=$1
      shift
      ;;
    -to | --timeout )
      shift
      TIMEOUT=$1
      shift
      ;;
    -t | --tag )
      shift
      TAG=$1
      shift
      ;;
    -l | --loop )
      shift
      firstchar=`echo $1 | cut -c1-1`
      if [ "$firstchar" = "-" ]; then
        LOOP=0
      elif [ "$firstchar" = "" ]; then
        LOOP=0
      else
        LOOP=$1
        shift
      fi
      ;;
    -i | --interactive )
      INTERACTIVE=true
      shift
      ;;
    -h | --help )
      echo $HELP
      exit
      ;;
    * )
      echo $HELP
      exit 1
  esac
done

docker_run

