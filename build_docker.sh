TAG=latest
IMAGE=dasmoorhuhn/atc-mithermometer-gateway
HELP="USAGE: sh build_docker.sh \n
[ -t | --tag ] Select a tag for building. Default is latest \n
[ -i | --image ] Select image tag for building. Default is dasmoorhuhn/atc-mithermometer-gateway \n
[ -h | --help ] Get this dialog"

docker version > /dev/null 2>&1
if [ "$?" != 0 ]; then
  echo Missing docker. Please install docker and try build again.
  exit 1
fi

docker_build(){
  docker build --tag $IMAGE:$TAG .
}

while [ "$1" != "" ]; do
  case $1 in
    -t | --tag )
      shift
      TAG=$1
      shift
      ;;
    -i | --image )
      shift
      IMAGE=$1
      shift
      ;;
    -h | --help )
      echo $HELP
      exit
      ;;
    * )
      echo $HELP
      exit 1
  esac
done

docker_build