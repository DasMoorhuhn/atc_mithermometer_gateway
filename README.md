<!-- TOC -->
* [ATC_MiThermometer_Gateway](#atc_mithermometer_gateway)
  * [Roadmap](#roadmap)
  * [Getting started](#getting-started)
  * [Preconditions](#preconditions)
  * [Shell Scripts](#shell-scripts)
  * [Start Gateway](#start-gateway)
  * [Build your own docker container](#build-your-own-docker-container)
  * [MicroPython for MicroController](#micropython-for-microcontroller)
* [Resources](#resources)
<!-- TOC -->

# ATC_MiThermometer_Gateway

Python gateway for the [custom firmware](https://github.com/atc1441/ATC_MiThermometer) for the [Xiaomi Thermometer LYWSD03MMC](https://www.mi.com/de/product/mi-temperature-and-humidity-monitor-2/).

![](.media/41N1IH9jwoL._AC_SL1024_.jpg)

## Roadmap

**Done:**
- Make in runnable in a docker container (because only cool people are using docker)
- Make docker image smaller. I mean shiiit 1GB D: should be possible to be under 500MB (It's now around 100MB)
- Implement a loop for fetching the data every X seconds

**TODOs:**
- [WIP] Can run on Raspberry Pi (3, 4, zero w) or any other Linux driven hardware which has BLE and WiFi support
- [WIP] Storing temperature, humidity and battery state as json in a text file
- [TODO] Make a microPython version for using the raspberry pico w or any other microcontroller with BLE and WiFi support
- [TODO] Collect data from multiple devices/gateways
- [TODO] Command line tool for managing the devices
- [TODO] Analyzing tool for making statistics
- [TODO] HomeAssistant integration
- [TODO] MQTT publishing
- [TODO] Maybe... a webinterface. But I suck at web stuff, so I don't know.
- [TODO] Implement other BLE Sensors

**Current State**

![](.media/demo.gif)

## Getting started

First of all, you need to flash the [custom firmware](https://github.com/atc1441/ATC_MiThermometer) on your LYWSD03MMC device. A step-by-step guid is in his youtube channel, the video is linked on his GitHub repo. It's straight forward and does not require any special hardware.

## Preconditions

Install `bluez`. It's needed for bluepy to communicate with the bluetooth adapter.
```bash
sudo apt-get install -y bluez
```

## Shell Scripts

**build_docker.sh**

| Arg           | Meaning                    | Default                               |
|---------------|----------------------------|---------------------------------------|
| -t \| --tag   | Set a tag for build        | latest                                |
| -i \| --image | Set a image name for build | dasmoorhuhn/atc-mithermometer-gateway |
| -h \| --help  | Get this help in the CLI   |                                       |

**run_docker.sh**

| Arg                 | Meaning                                                                                                                | Default              |
|---------------------|------------------------------------------------------------------------------------------------------------------------|----------------------|
| -d                  | Run in Backgrund                                                                                                       | false                |
| -t \| --tag         | Set a docker tag                                                                                                       | latest               |
| -b \| --build       | Build the image before running the container                                                                           | false                |
| -l \| --loop        | Start the gateway in looping mode                                                                                      | false (40s fallback) |
| -i \| --interactive | Start the container in interactive mode. That means, you can read the console output in real time and abort via STRG+C | false                |
| -a \| --api         | Start with the API                                                                                                     | false                |
| -v \| --volume      | Set the volume, where the data from the gateway will be stored. Use relative path like /home/user/gateway/data         | data                 |
| -tz \| --timezone   | Set the timezone                                                                                                       | Europe/Berlin        |
| -to \| --timeout    | Set the timeout for the bluetooth scan                                                                                 | 20                   |
| -h \| --help        | Get this dialog in CLI                                                                                                 |                      |
| --debug             | Activate debug mode. Meant for fixing errors or development.                                                           | false                |

## Start Gateway

For getting started, you need to download the `run_gateway.sh` or build the docker run commands by your own.

Run Gateway
```bash
sh run_gateway.sh
```

Run Gateway with specified volume for persistence data, api, loop interval of 40 seconds and interactive mode
```bash
sh run_gateway.sh --volume $PWD/data --loop 40 --interactive --api
```

## Build your own docker container

Build for your current platform (Without parameters, default values will be chosen)
```bash
sh build_docker.sh --image you/your-image-name you/your-image-name
```

For building the docker container for multiple platforms at once, you need to have [docker buildx](https://github.com/docker/buildx) installed. (Without parameters, default values will be chosen)
```bash
sh build_docker_multi_platforn.sh --platforms linux/amd64,linux/arm64 --image you/your-image-name you/your-image-name
```


## MicroPython for MicroController

Coming when I develop it...


# Resources
- https://pythonspeed.com/articles/alpine-docker-python this article is nuts :D
- https://docs.docker.com/build/building/multi-stage/
- https://github.com/jholtmann/ip_discovery